﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriceUaProject
{
    class ExcelManager
    {
        public ExcelManager()
        {

        }
        /// <summary>
        /// Метод работы с экселем
        /// </summary>
        /// <param name="FileName"></param>
        public static Product[] ReadExcel(string FileName)
        {
            try
            {
                var fi = new FileInfo(FileName);
                using (var package = new ExcelPackage(fi))
                {
                    using (ExcelWorksheet workSheet = package.Workbook.Worksheets[1])
                    {
                        Product[] products = new Product[workSheet.Dimension.End.Row - 2];
                        int countProduct = 0;
                        // Выгрузка всех значений
                        var list1 = workSheet.Cells[workSheet.Dimension.Start.Row + 2, 1, workSheet.Dimension.End.Row, 13].ToList();

                        for (int i = 0; i < list1.Count; i += 13)
                        {
                            string str = list1[i].Text;
                            products[countProduct] = new Product();
                            products[countProduct].idNumber = list1[i].Text;
                            products[countProduct].uniqueIdentificatorNom = list1[i + 1].Text;
                            products[countProduct].uniqueIdentificatorChara = list1[i + 2].Text;
                            products[countProduct].productName = list1[i + 3].Text;
                            products[countProduct].sizeAndIndex = list1[i + 4].Text;
                            products[countProduct].nameAndModel = list1[i + 5].Text;
                            products[countProduct].mm = products[countProduct].nameAndModel.Substring(products[countProduct].nameAndModel.LastIndexOf(' ') + 1); ;
                            products[countProduct].country = list1[i + 6].Text;
                            products[countProduct].year = list1[i + 7].Text;
                            if (list1[i + 8].Text.Count() > 0)
                                products[countProduct].lastPrice = Convert.ToDouble(list1[i + 8].Text);
                            if (list1[i + 9].Text.Count() > 0)
                                products[countProduct].procentPrice = Convert.ToDouble(list1[i + 9].Text);
                            if (list1[i + 10].Text.Count() > 0)
                                products[countProduct].price = Convert.ToDouble(list1[i + 10].Text);

                            products[countProduct].unitsMeasurement = list1[i + 11].Text;
                            products[countProduct].uniqueUnitsMeasurement = list1[i + 12].Text;
                            countProduct++;
                        }
                        Console.WriteLine(" ");
                        return products;
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("Error read xslx!!!      " + ex.Message.ToString());
            }
            return null;
        }


        public static void WriteExcel(Product product)
        {
            try
            {
                var fi = new FileInfo("result.xlsx");
                using (var package = new ExcelPackage(fi))
                {
                    int indexRows = 1;
                    using (var workSheet = package.Workbook.Worksheets[1])
                    {
                        try
                        {
                            var i = workSheet.Dimension.End.Row;
                            indexRows = workSheet.Dimension.End.Row;
                            if (indexRows > 0) indexRows++;
                        }
                        catch { }
                        workSheet.InsertRow(indexRows, 1);
                        workSheet.Cells[indexRows, 1].Value = product.idNumber;
                        workSheet.Cells[indexRows, 2].Value = product.uniqueIdentificatorNom;
                        workSheet.Cells[indexRows, 3].Value = product.uniqueIdentificatorChara;
                        workSheet.Cells[indexRows, 4].Value = product.productName;
                        workSheet.Cells[indexRows, 5].Value = product.sizeAndIndex;
                        workSheet.Cells[indexRows, 6].Value = product.nameAndModel;
                        workSheet.Cells[indexRows, 7].Value = product.country;
                        workSheet.Cells[indexRows, 8].Value = product.year;
                        workSheet.Cells[indexRows, 9].Value = product.lastPrice;
                        workSheet.Cells[indexRows, 10].Value = product.procentPrice;
                        workSheet.Cells[indexRows, 11].Value = product.price;
                        workSheet.Cells[indexRows, 12].Value = product.unitsMeasurement;
                        workSheet.Cells[indexRows, 13].Value = product.uniqueUnitsMeasurement;
                        workSheet.Cells[indexRows, 14].Value = product.priceFrom;
                        workSheet.Cells[indexRows, 15].Value = product.priceUp;
                        workSheet.Cells[indexRows, 16].Value = product.averagePrice;
                        workSheet.Cells[indexRows, 17].Value = product.countP;
                        workSheet.Cells[indexRows, 18].Value = product.url;
                        package.Save();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
